import Printer.Printer;
import Task1.*;
import Tsak2.CustomLinkedList;
public class Main {
    public static void main(String[] args) {
        CustomArrayList customArrayList = new CustomArrayList();

        System.out.println(customArrayList);
        customArrayList.add(1);
        customArrayList.add(10);
        System.out.println(customArrayList);
        customArrayList.removeByValue(10);
        System.out.println(customArrayList);
        customArrayList.removeByValue(1);
        System.out.println(customArrayList);
        customArrayList.add(100);
        System.out.println(customArrayList);
        customArrayList.replace(8, 5);
        System.out.println(customArrayList);


        CustomLinkedList list = new CustomLinkedList();
        Printer printer = new Printer();
        list.add(3);
        list.add(9);
        list.add(4);
        list.add(10);
        list.add(50);
        list.add(43);
        printer.print(list);
        list.add(2,100);
        printer.print(list);
        list.replace(2, 45);
        printer.print(list);
        list.removeByValue(45);
        printer.print(list);
        list.removeByIndex(1);



    }


}
