package Tsak2;
public class CustomLinkedList {

    private Node head;
    private int size;


    public CustomLinkedList() {
        Node node = new Node(0, null);
        this.head = node;
    }

    public void add(int value){
        Node tail = getNodeByIndex(size-1);
        Node node = new Node(value, null);
        tail.setNext(node);
        size++;
    }

    public void add(int index, int value){
        checkIndex(index);
        Node previous = getNodeByIndex(index-1);
        Node next = previous.getNext();
        Node node = new Node(value, next);
        previous.setNext(node);
        size++;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size){
            throw new IndexOutOfBoundsException("Size is: " + size + ", index: " + index);
        }
    }

    public void removeByValue(int value){
        Node current = head.getNext();
        Node previous = head;
        while(current != null){
            if(current.value == value){
                Node next = previous.getNext().getNext();
                previous.setNext(next);
                size--;
            }
            previous = current;
            current = current.getNext();
        }
    }

    public void removeByIndex(int index){
        checkIndex(index);
        Node previous = getNodeByIndex(index - 1);
        Node next = previous.getNext().getNext();
        previous.setNext(next);
        size--;
    }

    public void replace( int index, int value){
        Node current = getNodeByIndex(index);
        current.value = value;
    }

    public int get(int index){
        checkIndex(index);
        Node current = getNodeByIndex(index);
        return current.value;
    }

    private Node getNodeByIndex(int index) {
        if(index == -1){
            return head;
        }

        Node current = head.next;
        for(int i = 0; i < index; i++){
            current = current.next;
        }
        return current;
    }

    public int getSize(){
        return size;
    }

    private class Node{
        private int value;
        private Node next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

        public int getValue() {
            return value;
        }
        public Node getNext() {
            return next;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }


    @Override
    public String toString() {
        return "CustomLinkedList{" +
                "head=" + head +
                ", size=" + size +
                '}';
    }
}



